#!/usr/bin/env bash
# *********************************************************************** #
# ************************ 启动脚本 ************************************** #
# *********************************************************************** #
# *********************************************************************** #
# *********************************************************************** #

#运行程序环境变量
JAR_HOME=$(pwd)/official-account-0.0.1.jar

DB=jdbc:h2:file:./db/wechat

STATE=false

#启动脚本
start(){
    nohup java -Xmx512m -Xms512m -Xmn256m -Xss256K  -jar "${JAR_HOME}" --spring.datasource.url=${DB} --spring.h2.console.enabled=${STATE} > /dev/null 2>&1 &
}

#停止脚本
stop() {
    # shellcheck disable=SC2009
    PID=$(ps x | grep "${JAR_HOME}" | grep -v "grep" | awk  '{print $1}')
    # shellcheck disable=SC2071
    if [[ ${PID} > 0 ]]; then
        echo "当前${JAR_HOME}进程ID为：${PID}"
        echo "stop......"
        kill -9 "${PID}"
        echo "ok"
    fi
}

#命令执行
case ${1} in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        stop
        start
    ;;
    *)
        echo "执行命令: start / stop"
    ;;
esac