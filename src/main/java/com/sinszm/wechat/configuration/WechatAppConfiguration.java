package com.sinszm.wechat.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 智慧医疗公众号配置参数
 * @author zhangran
 * @date 2020-02-22
 */
@Configuration
@ConfigurationProperties("gftech.public")
public class WechatAppConfiguration {

    private String appid;

    private String secret;

    private String token;

    private String signkey;

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSignkey() {
        return signkey;
    }

    public void setSignkey(String signkey) {
        this.signkey = signkey;
    }
}
