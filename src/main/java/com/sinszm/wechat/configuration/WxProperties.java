package com.sinszm.wechat.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 系统自定义配置
 *
 * @author chenjianbo
 * @date 2020-3-8
 */
@ConfigurationProperties("wx")
public class WxProperties {

    /**
     * 系统接口授权账号
     */
    private String account = "admin";

    /**
     * 系统接口授权账号对应授权码
     */
    private String secret = "123456";

    /**
     * 重定向到微信获取code
     */
    private String api;

    /**
     * 微信利用code换取openid
     */
    private String token;

    /**
     * 网页授权域名是否已经开启https
     */
    private boolean ssl = false;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public boolean isSsl() {
        return ssl;
    }

    public void setSsl(boolean ssl) {
        this.ssl = ssl;
    }
}
