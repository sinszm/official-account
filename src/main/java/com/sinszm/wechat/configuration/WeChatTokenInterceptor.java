package com.sinszm.wechat.configuration;

import com.sinszm.common.util.CommonUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 公司状态检查
 * @author zhangran
 * @date 2020-03-24
 */
@Component
public class WeChatTokenInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(WeChatTokenInterceptor.class);
    private WechatAppConfiguration wechatAppConfiguration;

    @Autowired
    public WeChatTokenInterceptor(WechatAppConfiguration wechatAppConfiguration) {
        this.wechatAppConfiguration = wechatAppConfiguration;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (handler instanceof HandlerMethod) {
            String appId = getParam(request,"appId");
            String timeStamp = getParam(request,"time");
            String sign = getParam(request,"sign");
            if (StringUtils.isBlank(sign)){
                logger.info("无有效签名！");
                return false;
            }
            String selfSign = doSign(appId,timeStamp,sign);
            boolean result = selfSign.equals(sign);
            logger.info("验证结果："+result);
            return result;
        }
        return super.preHandle(request, response, handler);
    }

    private String getParam(HttpServletRequest request, String name){
        String value = request.getParameter(name);
        if (StringUtils.isEmpty(value)) {
            value = request.getHeader(name);
        }
        return value;
    }

    private String doSign(String appId, String time, String sign){
        return CommonUtils.SHA1(appId+time+wechatAppConfiguration.getSignkey());
    }
}
