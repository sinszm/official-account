package com.sinszm.wechat.controller;

import com.sinszm.common.Response;
import com.sinszm.wechat.service.InitService;
import com.sinszm.wechat.service.support.UserInitReqDto;
import com.sinszm.wechat.service.support.WechatInfoDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 初始化配置
 * @author chenjianbo
 * @date 2020-3-8
 */
@RestController
@RequestMapping(value = "/api/v1/init", produces = MediaType.APPLICATION_JSON_VALUE)
public class InitController {

    @Resource
    private InitService initService;

    /**
     * 配置公众号配置信息
     * @param dto
     * @param admin
     * @param adminSecret
     * @return
     */
    @PostMapping("/configWx")
    public Response<Object> configWx(
            @RequestBody(required = false)WechatInfoDto dto,
            @RequestHeader(value = "admin", defaultValue = "")String admin,
            @RequestHeader(value = "adminSecret", defaultValue = "")String adminSecret
    ) {
        initService.createWechatInfo(dto, admin, adminSecret);
        return Response.success("");
    }

    /**
     * 配置接入用户信息
     * @param dto
     * @param admin
     * @param adminSecret
     * @return
     */
    @PostMapping("/configUser")
    public Response<Object> configUser(
            @RequestBody(required = false) UserInitReqDto dto,
            @RequestHeader(value = "admin", defaultValue = "")String admin,
            @RequestHeader(value = "adminSecret", defaultValue = "")String adminSecret
    ) {
        return Response.success(
                initService.createUserInfo(dto, admin, adminSecret)
        );
    }

}
