package com.sinszm.wechat.controller;

import com.sinszm.wechat.service.WeChatTokenService;
import com.sinszm.wechat.service.support.WxInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信相关安全码管理
 * @author zhangran
 * @date 2020-03-28
 */
@RestController
@RequestMapping(value = "/api/wx", produces = MediaType.APPLICATION_JSON_VALUE)
public class WeChatTokenController {

    private WeChatTokenService weChatTokenService;

    @Autowired
    public WeChatTokenController(WeChatTokenService weChatTokenService) {
        this.weChatTokenService = weChatTokenService;
    }

    /**
     * 获取accessToken
     * @return string
     */
    @RequestMapping(value = "/accessToken", produces = MediaType.APPLICATION_JSON_VALUE)
    public String accessToken(@RequestParam("appId")String appId){
        return weChatTokenService.accessToken(appId);
    }

    /**
     * 用户使用微信登陆时根据code获取用户信息
     * @param code code
     * @return 用户信息
     */
    @RequestMapping(value = "/loginUserInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public WxInfoDto loginUserInfo(@RequestParam("appId") String appId,
                                   @RequestParam("code") String code){
        return weChatTokenService.loginUserInfo(appId, code);
    }

    /**
     * 根据登陆code获取openId
     * @param code code
     * @return String
     */
    @RequestMapping(value = "/getOpenId", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getOpenId(@RequestParam("appId")String appId,
                            @RequestParam("code")String code){
        return weChatTokenService.getOpenId(appId, code);
    }

    /**
     * 根据openId获取用户信息
     * @param openId openId
     * @return 用户信息
     */
    @RequestMapping(value = "/getWxUserInfo", produces = MediaType.APPLICATION_JSON_VALUE)
    public WxInfoDto getWxUserInfo(@RequestParam("appId")String appId,
                                   @RequestParam("openId") String openId){
        return weChatTokenService.getWxUserInfo(appId, openId);
    }

    /**
     * 获取jsapi ticket
     * @return string
     */
    @RequestMapping(value = "/ticket", produces = MediaType.APPLICATION_JSON_VALUE)
    public String ticket(@RequestParam("appId")String appId){
        return weChatTokenService.ticket(appId);
    }
}
