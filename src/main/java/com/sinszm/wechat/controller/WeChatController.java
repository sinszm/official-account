package com.sinszm.wechat.controller;

import com.sinszm.wechat.service.WeChatService;
import com.sinszm.wechat.util.Constant;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;

import static com.sinszm.wechat.util.Constant.SECRET_CODE;
import static com.sinszm.wechat.util.Constant.SECRET_RETURN;


/**
 * 进行授权和重定向处理
 *
 * @author chenjianbo
 * @date 2020-3-8
 */
@Controller
@RequestMapping(value = Constant.SECRET_PREFIX, produces = MediaType.APPLICATION_JSON_VALUE)
public class WeChatController {

    @Resource
    private WeChatService weChatService;

    /**
     * 获取用户传入验证参数
     *
     * @param userId        用户ID
     * @param userSecret    用户授权码
     * @return              重定向
     */
    @RequestMapping(value = SECRET_CODE, method = {RequestMethod.POST, RequestMethod.GET})
    public String code(
            @RequestParam("userId")String userId,
            @RequestParam("userSecret")String userSecret
    ) {
        return weChatService.code(userId, userSecret);
    }

    /**
     * 根据响应结果进行逻辑处理
     *
     * @param code      授权码
     * @param state     用户ID
     * @return          重定向
     */
    @RequestMapping(value = SECRET_RETURN, method = {RequestMethod.POST, RequestMethod.GET})
    public String result(
            @RequestParam("code")String code,
            @RequestParam("state")String state
    ) {
        return weChatService.result(code, state);
    }

}
