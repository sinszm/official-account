package com.sinszm.wechat.dao;

import com.sinszm.wechat.dao.entity.DbStartupLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA
 * @author chenjianbo
 * @date 2020-3-8
 */
public interface DbStartupLogRepository extends JpaRepository<DbStartupLog, Integer> {
}
