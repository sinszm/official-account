package com.sinszm.wechat.dao;

import com.sinszm.wechat.dao.entity.DbWeChatCommon;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA
 * @author chenjianbo
 * @date 2020-3-8
 */
public interface DbWeChatCommonRepository extends JpaRepository<DbWeChatCommon, String> {

    /**
     * 根据应用ID查询信息
     * @param appId
     * @return
     */
    DbWeChatCommon getByAppId(String appId);

}
