package com.sinszm.wechat.dao;

import com.sinszm.wechat.dao.entity.DbWeChatAppRel;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * JPA
 * @author chenjianbo
 * @date 2020-3-8
 */
public interface DbWeChatAppRelRepository extends JpaRepository<DbWeChatAppRel, String> {

    /**
     * 根据用户ID进行统计判断是否存在该账号
     * @param userId
     * @return
     */
    long countByUserId(String userId);

    /**
     * 根据用户ID和授权码查询用户绑定信息
     * @param userId
     * @param userSecret
     * @return
     */
    DbWeChatAppRel getByUserIdAndUserSecret(String userId, String userSecret);

}
