package com.sinszm.wechat.dao.entity;

import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 微信公众号全局配置
 *
 * @author chenjianbo
 */
@NoRepositoryBean
@Entity
@Table(name = "db_wechat_common", indexes = {
        @Index(name = "i_db_we_chat_common_app_id", columnList = "app_id", unique = true)
})
public class DbWeChatCommon implements Serializable {

    private static final long serialVersionUID = 6217497286094405547L;

    @Id
    private String id;

    /**
     * 公众号应用ID
     */
    @Column(name = "app_id", length = 50, nullable = false)
    private String appId;

    /**
     * 公众号应用安全码
     */
    @Column(name = "app_secret", length = 50, nullable = false)
    private String appSecret;

    /**
     * 业务域名
     */
    @Column(name = "business_domain", length = 100, nullable = false)
    private String businessDomain;

    /**
     * JS安全域名
     */
    @Column(name = "js_domain", length = 100, nullable = false)
    private String jsDomain;

    /**
     * 网页授权域名
     */
    @Column(name = "auth_domain", length = 100, nullable = false)
    private String authDomain;

    /**
     * 消息接管服务器配置URL
     */
    @Column(name = "msg_url", length = 100)
    private String msgUrl;

    /**
     * 消息接管服务器配置对应令牌TOKEN
     */
    @Column(name = "msg_token", length = 100)
    private String msgToken;

    @Column(name = "create_time")
    private Date createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getBusinessDomain() {
        return businessDomain;
    }

    public void setBusinessDomain(String businessDomain) {
        this.businessDomain = businessDomain;
    }

    public String getJsDomain() {
        return jsDomain;
    }

    public void setJsDomain(String jsDomain) {
        this.jsDomain = jsDomain;
    }

    public String getAuthDomain() {
        return authDomain;
    }

    public void setAuthDomain(String authDomain) {
        this.authDomain = authDomain;
    }

    public String getMsgUrl() {
        return msgUrl;
    }

    public void setMsgUrl(String msgUrl) {
        this.msgUrl = msgUrl;
    }

    public String getMsgToken() {
        return msgToken;
    }

    public void setMsgToken(String msgToken) {
        this.msgToken = msgToken;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return Json.toJson(this, JsonFormat.tidy());
    }
}
