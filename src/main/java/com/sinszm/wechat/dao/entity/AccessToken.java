package com.sinszm.wechat.dao.entity;

import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * 各种类型的微信accessToken
 * @author zhangran
 * @date 2020-03-28
 */
@NoRepositoryBean
@Entity
@Table(name = "db_wechat_access_token")
public class AccessToken {
    /**
     * 主键id
     */
    @Id
    private String id;
    /**
     * 值
     */
    private String value;
    /**
     * 过期时间
     */
    private Date overdueTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getOverdueTime() {
        return overdueTime;
    }

    public void setOverdueTime(Date overdueTime) {
        this.overdueTime = overdueTime;
    }
}
