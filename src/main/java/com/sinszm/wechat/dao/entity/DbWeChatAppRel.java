package com.sinszm.wechat.dao.entity;

import com.sinszm.wechat.dao.entity.support.Module;
import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 微信公众号用户关联配置授权
 *
 * @author chenjianbo
 */
@NoRepositoryBean
@Entity
@Table(name = "db_wechat_app_rel", indexes = {
        @Index(name = "i_db_wechat_app_rel_user_id", columnList = "user_id")
})
public class DbWeChatAppRel implements Serializable {

    private static final long serialVersionUID = 6217497286094405547L;

    @Id
    private String id;

    /**
     * 用户ID
     */
    @Column(name = "user_id", length = 50, nullable = false, unique = true)
    private String userId;

    /**
     * 用户授权码
     */
    @Column(name = "user_secret", length = 50, nullable = false)
    private String userSecret;

    /**
     * 关联公众号ID
     */
    @Column(name = "app_id", length = 50, nullable = false)
    private String appId;

    /**
     * 重定向到用户的接口地址
     */
    @Column(name = "redirect_address", nullable = false)
    private String redirectAddress;

    /**
     * 授权模型
     */
    @Column(name = "module", nullable = false)
    @Enumerated(EnumType.STRING)
    private Module module;

    @Column(name = "create_time")
    private Date createTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserSecret() {
        return userSecret;
    }

    public void setUserSecret(String userSecret) {
        this.userSecret = userSecret;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRedirectAddress() {
        return redirectAddress;
    }

    public void setRedirectAddress(String redirectAddress) {
        this.redirectAddress = redirectAddress;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return Json.toJson(this, JsonFormat.tidy());
    }
}
