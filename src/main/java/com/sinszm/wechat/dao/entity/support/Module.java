package com.sinszm.wechat.dao.entity.support;

/**
 * 授权模型
 * @author chenjianbo
 * @date 2020-3-8
 */
public enum Module {

    /**
     * 作用域
     * <p>
     *     不弹出授权页面，直接跳转，只能获取用户openid
     * </p>
     */
    BASE("snsapi_base"),

    /**
     * 作用域
     * <p>
     *     弹出授权页面，可通过openid拿到昵称、性别、所在地。并且， 即使在未关注的情况下，只要用户授权，也能获取其信息
     * </p>
     */
    USERINFO("snsapi_userinfo");

    private String value;

    Module(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
