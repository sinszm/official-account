package com.sinszm.wechat.dao.entity;

import org.nutz.json.Json;
import org.nutz.json.JsonFormat;
import org.springframework.data.repository.NoRepositoryBean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 项目启动日志
 * @author chenjianbo
 */
@NoRepositoryBean
@Entity
@Table(name = "db_startup_log")
public class DbStartupLog implements Serializable {

    private static final long serialVersionUID = 8616445688360957934L;

    @Id
    private int id;

    private Date createTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return Json.toJson(this, JsonFormat.tidy());
    }
}
