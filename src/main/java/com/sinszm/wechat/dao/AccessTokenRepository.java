package com.sinszm.wechat.dao;

import com.sinszm.wechat.dao.entity.AccessToken;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * accesstoken 数据操作
 * @author zhangran
 * @date 2020-03-28
 */
public interface AccessTokenRepository extends JpaRepository<AccessToken,String> {
}
