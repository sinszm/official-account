package com.sinszm.wechat.util;

/**
 * 常量
 * @author chenjianbo
 * @date 2020-3-8
 */
public interface Constant {

    String SECRET_PREFIX = "/api/v1/secret";

    String SECRET_CODE = "/code";

    String SECRET_RETURN = "/content";

    String SECRET_URI = SECRET_PREFIX + SECRET_RETURN;

}
