package com.sinszm.wechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动
 * @author chenjianbo
 */
@SpringBootApplication
public class OfficialAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(OfficialAccountApplication.class, args);
    }

}
