package com.sinszm.wechat.service;

/**
 * 重定向业务处理
 * @author chenjianbo
 * @date 2020-3-8
 */
public interface WeChatService {

    /**
     * 验证合法参数
     * @param userId
     * @param userSecret
     * @return
     */
    String code(String userId, String userSecret);

    /**
     * 结果处理并重定向到用户界面
     * @param code
     * @param state
     * @return
     */
    String result(String code, String state);
}
