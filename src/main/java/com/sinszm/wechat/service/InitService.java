package com.sinszm.wechat.service;

import com.sinszm.wechat.service.support.UserInitReqDto;
import com.sinszm.wechat.service.support.WechatInfoDto;

/**
 * 初始化
 * @author chenjianbo
 * @date 2020-3-8
 */
public interface InitService {

    /**
     * 创建公众号配置
     *
     * @param dto
     * @param admin
     * @param adminSecret
     */
    void createWechatInfo(WechatInfoDto dto, String admin, String adminSecret);

    /**
     * 创建接入用户
     * @param dto
     * @param admin
     * @param adminSecret
     * @return
     */
    Object createUserInfo(UserInitReqDto dto, String admin, String adminSecret);
}
