package com.sinszm.wechat.service.support;

import java.io.Serializable;

/**
 * 微信接口调用返回体
 * @author zhangran
 * @date 2020-02-22
 */
public class WxRespDto implements Serializable {
    private static final long serialVersionUID = -3629946073336214935L;
    private Integer errcode;

    private String errmsg;

    public Integer getErrcode() {
        return errcode;
    }

    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
}
