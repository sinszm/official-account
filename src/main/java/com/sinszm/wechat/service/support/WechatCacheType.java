package com.sinszm.wechat.service.support;

/**
 * 微信公众平台缓存类型
 * @author zhangran
 * @date 2020-03-09
 */
public enum WechatCacheType {
    /**
     *
     */
    ACCESS_TOKEN,
    SPECIAL_ACCESS_TOKEN,
    JSAPI_TICKET,
    REFRESH_TOKEN
}
