package com.sinszm.wechat.service.support;

/**
 * 创建公众号
 * @author chenjianbo
 * @date 2020-3-8
 */
public class WechatInfoDto {

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 应用授权码
     */
    private String appSecret;

    /**
     * 网页授权域名
     */
    private String domain;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }
}
