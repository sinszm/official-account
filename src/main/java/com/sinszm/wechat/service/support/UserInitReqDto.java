package com.sinszm.wechat.service.support;

import com.sinszm.wechat.dao.entity.support.Module;

/**
 * 初始化用户
 * @author chenjianbo
 * @date 2020-3-8
 */
public class UserInitReqDto {

    /**
     * 应用ID
     */
    private String appId;

    /**
     * 回调地址
     */
    private String redirectAddress;

    /**
     * 授权模型
     */
    private Module module;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRedirectAddress() {
        return redirectAddress;
    }

    public void setRedirectAddress(String redirectAddress) {
        this.redirectAddress = redirectAddress;
    }

    public Module getModule() {
        return module;
    }

    public void setModule(Module module) {
        this.module = module;
    }
}
