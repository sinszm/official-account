package com.sinszm.wechat.service.support;


/**
 * accessToken调用返回
 * @author zhangran
 * @date 2020-02-22
 */
public class WxAccessTokenRespDto extends WxRespDto {
    private static final long serialVersionUID = 7225554527221919046L;
    private String access_token;

    private Long expires_in;

    private String ticket;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Long getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Long expires_in) {
        this.expires_in = expires_in;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }
}
