package com.sinszm.wechat.service.support;

import com.sinszm.common.exception.ApiError;

/**
 * 系统错误代码
 *
 * @author chenjianbo
 */
public enum NspWechatError implements ApiError {
    /**
     *
     */
    NSP_WECHAT_ERROR_01("获取ACCESSTOKEN失败"),
    NSP_WECHAT_ERROR_02("获取OPENID失败"),
    NSP_WECHAT_ERROR_03("获取用户信息失败"),
    NSP_WECHAT_ERROR_04("微信支付证书加载错误"),
    NSP_WECHAT_ERROR_05("XML转换失败"),
    NSP_WECHAT_ERROR_06("微信支付网络或请求方式异常"),
    NSP_WECHAT_ERROR_07("获取支付订单凭据异常"),

    NSP_WECHAT_ERROR_08("商户未配置appid"),
    NSP_WECHAT_ERROR_09("商户未配置app密钥"),
    NSP_WECHAT_ERROR_10("商户未配置商户号"),
    NSP_WECHAT_ERROR_11("商户未配置商户密钥"),
    NSP_WECHAT_ERROR_12("证书文件不存在"),
    NSP_WECHAT_ERROR_13("获取商户支付配置失败"),

    NSP_WECHAT_ERROR_14("模板消息发送失败"),
    NSP_WECHAT_ERROR_15("订单关闭时间不能为空"),
    NSP_WECHAT_ERROR_16("业务名称不能为空"),

    NSP_WECHAT_ERROR_17("获取TICKET失败"),
    ;

    private String message;

    NspWechatError(String message) {
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.name();
    }

    @Override
    public String getMessage() {
        return message;
    }

}
