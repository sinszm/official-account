package com.sinszm.wechat.service;

import com.sinszm.wechat.service.support.WxInfoDto;

/**
 * 微信账号认证相关
 * @author zhangran
 * @date 2020-02-20
 */
public interface WeChatTokenService {

    /**
     * 获取accessToken
     * @return string
     */
    String accessToken(String appId);

    /**
     * 用户使用微信登陆时根据code获取用户信息
     * @param code code
     * @return 用户信息
     */
    WxInfoDto loginUserInfo(String appId, String code);

    /**
     * 根据登陆code获取openId
     * @param code code
     * @return String
     */
    String getOpenId(String appId, String code);

    /**
     * 根据openId获取用户信息
     * @param openId openId
     * @return 用户信息
     */
    WxInfoDto getWxUserInfo(String appId, String openId);

    /**
     * 获取jsapi ticket
     * @return string
     */
    String ticket(String appId);
}
