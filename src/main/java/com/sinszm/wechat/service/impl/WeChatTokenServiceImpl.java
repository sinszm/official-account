package com.sinszm.wechat.service.impl;

import com.fcibook.quick.http.QuickHttp;
import com.sinszm.common.exception.ApiException;
import com.sinszm.wechat.configuration.WechatAppConfiguration;
import com.sinszm.wechat.dao.AccessTokenRepository;
import com.sinszm.wechat.dao.DbWeChatCommonRepository;
import com.sinszm.wechat.dao.entity.AccessToken;
import com.sinszm.wechat.dao.entity.DbWeChatCommon;
import com.sinszm.wechat.service.WeChatTokenService;
import com.sinszm.wechat.service.support.*;
import org.nutz.json.Json;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;

import static com.sinszm.wechat.service.support.NspWechatError.*;
import static com.sinszm.wechat.service.support.WechatConstant.*;

/**
 * 微信accessToken代理
 * @author zhangran
 * @date 2020-03-28
 */
@Service
public class WeChatTokenServiceImpl implements WeChatTokenService {

    private AccessTokenRepository accessTokenRepository;
    private WechatAppConfiguration wechatAppConfiguration;
    private DbWeChatCommonRepository dbWeChatCommonRepository;

    @Autowired
    public WeChatTokenServiceImpl(AccessTokenRepository accessTokenRepository,
                                  WechatAppConfiguration wechatAppConfiguration,
                                  DbWeChatCommonRepository dbWeChatCommonRepository) {
        this.accessTokenRepository = accessTokenRepository;
        this.wechatAppConfiguration = wechatAppConfiguration;
        this.dbWeChatCommonRepository = dbWeChatCommonRepository;
    }

    private String id(WechatCacheType type, String appId){
        return appId+type;
    }

    private String get(WechatCacheType type, String appId){
        String key = id(type,appId);
        AccessToken item = accessTokenRepository.findById(key).orElse(null);
        if (item==null){
            return null;
        }
        if (new Date().after(item.getOverdueTime())){
            return null;
        }
        return item.getValue();
    }

    private void save(WechatCacheType type, String appId, String accessToken, long expireTime) {
        String key = id(type,appId);
        AccessToken item = accessTokenRepository.findById(key).orElse(null);
        if (item==null){
            item = new AccessToken();
            item.setId(key);
        }
        item.setValue(accessToken);
        item.setOverdueTime(new Date(System.currentTimeMillis()+expireTime*1000));
        accessTokenRepository.save(item);
    }

    private DbWeChatCommon defaultConfig;

    private DbWeChatCommon getConfig(String appId){
        if (wechatAppConfiguration.getAppid().equals(appId)){
            if (defaultConfig!=null){
                return defaultConfig;
            }else {
                DbWeChatCommon config = new DbWeChatCommon();
                config.setAppId(wechatAppConfiguration.getAppid());
                config.setAppSecret(wechatAppConfiguration.getSecret());
                config.setMsgToken(wechatAppConfiguration.getToken());
                defaultConfig = config;
                return config;
            }
        }
        DbWeChatCommon common = dbWeChatCommonRepository.getByAppId(appId);
        if (common == null) {
            throw new ApiException("CODE-ERR-03", "公众号配置信息未找到");
        }
        return common;
    }

    @Override
    public String accessToken(String appId){
        return credential(appId,WechatCacheType.ACCESS_TOKEN);
    }

    @Override
    public String ticket(String appId){
        return credential(appId, WechatCacheType.JSAPI_TICKET);
    }

    @Override
    public WxInfoDto loginUserInfo(String appId, String code) {
        String openId = getOpenId(appId, code);
        return getWxInfoUseCgiMode(appId, openId);
    }

    @Override
    public String getOpenId(String appId, String code){
        DbWeChatCommon config = getConfig(appId);
        WxOpenIdRespDto respDto = getFromJson(String.format(OPEN_ID_URL,appId,config.getAppSecret(),code),WxOpenIdRespDto.class);
        if (isSuccess(respDto)){
            save(WechatCacheType.REFRESH_TOKEN,appId,respDto.getAccess_token(),30*24*60*60);
            save(WechatCacheType.SPECIAL_ACCESS_TOKEN,appId,respDto.getAccess_token(),respDto.getExpires_in()/10*9);
            return respDto.getOpenid();
        }else {
            if (respDto!=null&&StringUtils.hasText(respDto.getErrmsg())){
                throw new ApiException(NSP_WECHAT_ERROR_02.getCode(),respDto.getErrmsg());
            }
            throw new ApiException(NSP_WECHAT_ERROR_02);
        }
    }

    @Override
    public WxInfoDto getWxUserInfo(String appId, String openId){
        String accessToken = accessToken(appId);
        WxInfoDto dto = getFromJson(String.format(USER_INFO_URL,accessToken,openId),WxInfoDto.class);
        checkResult(dto);
        //用户是否关注了公众号
        String has = "0";
        if (has.equals(dto.getSubscribe())){
            dto = getWxInfoUseCgiMode(appId,openId);
        }
        return dto;
    }

    /**
     * 以cgi mode 获取用户信息 此时用户无需关注公众号
     * @param openId openId
     * @return dto
     */
    private WxInfoDto getWxInfoUseCgiMode(String appId, String openId){
        WxInfoDto dto = getFromJson(String.format(PAGE_USER_URL,credential(appId,WechatCacheType.SPECIAL_ACCESS_TOKEN),openId),WxInfoDto.class);
        checkResult(dto);
        return dto;
    }

    /**
     * 获取请求微信接口用的凭据
     * @param type 凭据类型
     * @return credential
     */
    private String credential(String appId, WechatCacheType type){
        String credential = get(type,appId);
        if (StringUtils.hasText(credential)){
            return credential;
        }
        DbWeChatCommon config = getConfig(appId);
        WxAccessTokenRespDto result;
        switch (type){
            case ACCESS_TOKEN:
                result = getFromJson(String.format(ACCESS_TOKEN_URL,appId,config.getAppSecret()),WxAccessTokenRespDto.class);
                break;
            case SPECIAL_ACCESS_TOKEN:
                result = getFromJson(String.format(SPECIAL_ACCESS_TOKEN_URL,appId,credential(appId,WechatCacheType.REFRESH_TOKEN)),WxAccessTokenRespDto.class);
                break;
            case JSAPI_TICKET:
                result = getFromJson(String.format(TICKET_URL,credential(appId,WechatCacheType.ACCESS_TOKEN)),WxAccessTokenRespDto.class);
                break;
            default:
                String refreshToken = get(WechatCacheType.REFRESH_TOKEN,appId);
                if (!StringUtils.hasText(refreshToken)){
                    throw new ApiException();
                }
                return refreshToken;
        }
        checkCredentialResult(type,result);
        credential = type == WechatCacheType.JSAPI_TICKET? result.getTicket():result.getAccess_token();
        save(type,appId,credential,result.getExpires_in()/10*9);
        return credential;
    }

    /**
     * 获取请求微信接口的返回值检查
     * @param type 请求类型
     * @param respDto 返回体
     */
    private void checkCredentialResult(WechatCacheType type, WxAccessTokenRespDto respDto){
        NspWechatError error = type == WechatCacheType.JSAPI_TICKET?NSP_WECHAT_ERROR_17:NSP_WECHAT_ERROR_01;
        if (respDto==null){
            throw new ApiException(error);
        }
        String credential = type == WechatCacheType.JSAPI_TICKET?respDto.getTicket():respDto.getAccess_token();
        if (!isSuccess(respDto)&&!StringUtils.hasText(credential)){
            if (StringUtils.hasText(respDto.getErrmsg())){
                throw new ApiException(error.getCode(),respDto.getErrmsg());
            }
            throw new ApiException(error);
        }
    }

    /**
     * 检查用户返回体的结果
     * @param dto dto
     */
    private void checkResult(WxInfoDto dto){
        if (!isSuccess(dto)){
            if (dto!=null&&StringUtils.hasText(dto.getErrmsg())){
                throw new ApiException(NSP_WECHAT_ERROR_03.getCode(),dto.getErrmsg());
            }
            throw new ApiException(NSP_WECHAT_ERROR_03);
        }
    }

    /**
     * 请求是否成功
     * @param dto basedto
     * @return boolean
     */
    private boolean isSuccess(WxRespDto dto){
        return dto!=null&&(dto.getErrcode()==null||dto.getErrcode()==0);
    }

    private <T> T getFromJson(String url, Class<T> clazz) {
        try {
            String result = new QuickHttp().url(url).get().text();
            return Json.fromJson(clazz,result);
        }catch (Exception e){
            throw new ApiException("NSP_WECHAT_ERROR",e.getMessage());
        }
    }
}
