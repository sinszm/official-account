create table if not exists db_startup_log (
    id int not null primary key auto_increment,
    create_time datetime
);